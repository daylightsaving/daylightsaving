﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour
{
    [HideInInspector]
    public bool facingRight = true;         // For determining which way the player is currently facing.
    [HideInInspector]
    public bool jump = false;               // Condition for whether the player should jump.


    public float moveForce = 7.5f; //365f;          // Amount of force added to move the player left and right.
    public float maxSpeed = 3.25f; //5f;             // The fastest the player can travel in the x axis.
    //public AudioClip[] jumpClips;           // Array of clips for when the player jumps.
    public float jumpForce = 267.5f; //1000f;         // Amount of force added when the player jumps.
    //public AudioClip[] taunts;              // Array of clips for when the player taunts.
    //public float tauntProbability = 50f;    // Chance of a taunt happening.
    //public float tauntDelay = 1f;           // Delay for when the taunt should happen.


    //private int tauntIndex;                 // The index of the taunts array indicating the most recent taunt.
    private Transform groundCheckA, groundCheckB;          // A position marking where to check if the player is grounded.
    private bool grounded = false;          // Whether or not the player is grounded.
    //private Animator animator;                  // Reference to the player's animator component.
    //private Rigidbody rb3d;

    void Start()
    {
        //rb3d = GetComponent<Rigidbody>();
    }

    void Awake()
    {
        // Setting up references.
        groundCheckA = transform.Find("GroundCheckA");
        groundCheckB = transform.Find("GroundCheckB");

        //animator = GetComponent<Animator>();
    }


    void Update()
    {
        // The player is grounded if a linecast to the groundcheck position hits anything on the ground layer.
        grounded = Physics.Linecast(transform.position, groundCheckA.position, 1 << LayerMask.NameToLayer("Ground")) || 
            Physics.Linecast(transform.position, groundCheckB.position, 1 << LayerMask.NameToLayer("Ground"));

        // If the jump button is pressed and the player is grounded then the player should jump.
        if (Input.GetButtonDown("Jump") && grounded)
            jump = true;
    }


    void FixedUpdate()
    {
        // Cache the horizontal input.
        float h = Input.GetAxis("Horizontal");

        // The Speed animator parameter is set to the absolute value of the horizontal input.
        //anim.SetFloat("Speed", Mathf.Abs(h));

        // If the player is changing direction (h has a different sign to velocity.x) or hasn't reached maxSpeed yet...
        if (h * GetComponent<Rigidbody>().velocity.x < maxSpeed)
            // ... add a force to the player.
            GetComponent<Rigidbody>().AddForce(Vector3.right * h * moveForce);

        // If the player's horizontal velocity is greater than the maxSpeed...
        if (Mathf.Abs(GetComponent<Rigidbody>().velocity.x) > maxSpeed)
            // ... set the player's velocity to the maxSpeed in the x axis.
            GetComponent<Rigidbody>().velocity = new Vector3(Mathf.Sign(GetComponent<Rigidbody>().velocity.x) * maxSpeed, GetComponent<Rigidbody>().velocity.y);

        // If the input is moving the player right and the player is facing left...
        if (h > 0 && !facingRight)
            // ... flip the player.
            Flip();
        // Otherwise if the input is moving the player left and the player is facing right...
        else if (h < 0 && facingRight)
            // ... flip the player.
            Flip();

        // If the player should jump...
        if (jump)
        {
            // Set the Jump animator trigger parameter.
            //animator.SetTrigger("Jump");

            // Play a random jump audio clip.
            //int i = Random.Range(0, jumpClips.Length);
            //AudioSource.PlayClipAtPoint(jumpClips[i], transform.position);

            // Add a vertical force to the player.
            float addJforce = 1.0f * Mathf.Abs(GetComponent<Rigidbody>().velocity.x);
            GetComponent<Rigidbody>().AddForce(new Vector3(0f, jumpForce + addJforce, 0f));
            //Debug.Log(jumpForce + addJforce);

            // Make sure the player can't jump again until the jump conditions from Update are satisfied.
            jump = false;
        }
    }


    void Flip()
    {
        // Switch the way the player is labelled as facing.
        facingRight = !facingRight;

        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    void OnTriggerEnter(Collider other)
    {
        //Check the provided Collider parameter other to see if it is tagged "LightOrb", if it is...
        if (other.gameObject.CompareTag("LightOrb"))
        {
            //... then set the other object we just collided with to inactive.
            StartCoroutine(OrbHideDelay(other));
            
            //other.gameObject.SetActive(false);

            //Add one to the current value of our count variable.
            //count = count + 1;

            //Update the currently displayed count by calling the SetCountText function.
            //SetCountText();
            
        }
    }

    IEnumerator OrbHideDelay(Collider orb)
    {
        yield return new WaitForSecondsRealtime(0.25f);
        orb.gameObject.SetActive(false);
        Debug.Log("OrbCollected!");
    }

    /*
    public IEnumerator Taunt()
    {
        // Check the random chance of taunting.
        float tauntChance = Random.Range(0f, 100f);
        if (tauntChance > tauntProbability)
        {
            // Wait for tauntDelay number of seconds.
            yield return new WaitForSeconds(tauntDelay);

            // If there is no clip currently playing.
            if (!GetComponent<AudioSource>().isPlaying)
            {
                // Choose a random, but different taunt.
                tauntIndex = TauntRandom();

                // Play the new taunt.
                GetComponent<AudioSource>().clip = taunts[tauntIndex];
                GetComponent<AudioSource>().Play();
            }
        }
    }
    */

    /*
    int TauntRandom()
    {
        // Choose a random index of the taunts array.
        int i = Random.Range(0, taunts.Length);

        // If it's the same as the previous taunt...
        if (i == tauntIndex)
            // ... try another random taunt.
            return TauntRandom();
        else
            // Otherwise return this index.
            return i;
    }
    */
}
