﻿using UnityEngine;
using System.Collections;
using System;

public class TransitionScript : Singleton<TransitionScript> {

    public GameObject transitionSprite;
    public BoxCollider2D transitionCollider;
   
    // Use this for initialization
    void Start () {

	}

    public void playTransition(float transitionTime,Action OnTransitionBegin, Action OnTransitionMid,Action OnTransitionEnd)
    {
        transitionCollider.enabled = true;
        if (OnTransitionBegin != null)
            OnTransitionBegin();
        LeanTween.alpha(transitionSprite, 1f, transitionTime / 2).setOnComplete(
            delegate () {
                if (OnTransitionMid != null)
                    OnTransitionMid();
                LeanTween.alpha(transitionSprite, 0f, transitionTime / 2).setOnComplete(
                    delegate ()
                    {
                        if (OnTransitionEnd != null)
                            OnTransitionEnd();
                        transitionCollider.enabled = false;
                    }
                    );
            }
            );
    }
}
