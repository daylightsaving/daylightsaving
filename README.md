### **Week 38** ###

**Mechanics:**

- Set Fixed Camera distance + Camera
- Basic character controls
- Sprite Animations
- Test environment scene build
- Temp background

**Others:**

- Check progress of each member
- Update Trello
- Upload early version with above ^ mechanics
- Make sure everyone gets access to the latest build
- Decide the game name?
- Update plans

### **Week 37** ###

**Project Starts!**


**Tasks done / decided:**

- Game Concept
- Roles decided
- Tasks handled
- Character concept
- Unity learning
- Register Bitbucket
- Create Facebook group
- Starting to implement basic features


This post will be kept updated for now.